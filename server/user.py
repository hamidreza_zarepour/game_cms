import json
import hashlib
import database as db
from webargs import fields
from webargs.flaskparser import use_args
from flask import Blueprint, request, session, g
from serverbase import return_json, ErrorToClient


bp = Blueprint('user', __name__)


def add_user(username, password, info):
    u = db.User()
    u.username = username
    u.password = password
    # u.role = info.get('role', None)
    # u.name = info.get('name', None)
    # u.email = info.get('email', None)
    g.db_session.add(u)
    g.db_session.commit()
    return u.id


@bp.route('/signup', methods=['POST'])
@use_args({'username': fields.Str(required=True), 'password': fields.Str(required=True)})
@return_json
def _signup(args):
    username = args['username']
    password = args['password']
    if g.db_session.query(db.User).filter_by(username=username).first() is not None:
        raise ErrorToClient('user already exists')
    password = hashlib.sha1(password).hexdigest()
    add_user(username, password, {})
    return True


@bp.route('/signin', methods=['POST'])
@use_args({'username': fields.Str(required=True), 'password': fields.Str(required=True)})
@return_json
def _signin(args):
    username = args['username']
    password = args['password']
    if username == 'Guest':
        password = 'h@m!dr(za'
    password = hashlib.sha1(password).hexdigest()
    rec = g.db_session.query(db.User).filter_by(username=username, password=password).first()
    if rec is None:
        session['user_id'] = None
        raise ErrorToClient('username or password not valid')
    session['user_id'] = rec.id
    return True


@bp.route('/signout', methods=['GET', 'POST'])
@return_json
def _signout():
    session['user_id'] = None
    return True


@bp.route('/issignin', methods=['GET', 'POST'])
@return_json
def _issignin():
    return g.user_id is not None
