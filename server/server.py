# encoding: utf8

import json
import config
import logging
import database as db
from webargs import fields
from webargs.flaskparser import use_args
from serverbase import return_json, ErrorToClient
from flask import Blueprint, request, make_response, g


bp = Blueprint('server', __name__)
logger = logging.getLogger(config.log.appname)


@bp.route('/add', methods=['POST'])
@use_args({'a': fields.Int(required=True), 'b': fields.Int(required=True)})
@return_json
def _add(args):
    logger.debug('args = %s', args)
    return args['a'] + args['b']


@bp.route('/error-test/<int:a>/<int:b>', methods=['GET'])
def _error_test(a, b):
    if b == 0:
        raise ErrorToClient('divisor cannot be zero')
    if a >= 100:
        raise ErrorToClient('divident should be less than 100', a)
    return str(int(a / b))


@bp.route('/user-check', methods=['GET'])
def _user_check():
    logger.debug('HI')
    if g.user_id is None:
        raise ErrorToClient('access denied')
    return 'hello'
