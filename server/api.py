import user
import server


def register_blueprints(app):
    app.register_blueprint(user.bp, url_prefix='/user')
    app.register_blueprint(server.bp, url_prefix='')
