server components
-----------------
flask, sqlalchemy, (+ user management)


ui components
-------------
angular, bootstrap, bootstrap-rtl, fontawesome, jquery, sugarjs, sugar-extra


how to install
--------------
server:
    cd server
    pip install -r requirements.txt
ui:
    cd ui
    bower install


how to run server
-----------------
python run.py


config
------
you can add a local_config.py file in server directory instead directly editing config.py
with a content like this:

import config
config.flask.port = 6000


how to signup a new user
------------------------
curl -d '{"username":"you","password":"a"}' -X POST -H 'content-type: application/json' http://localhost:5000/user/signup


how to signin
-------------
curl -v -d '{"username":"you","password":"a"}' -X POST -H 'content-type: application/json' http://localhost:5000/user/signin
then use session cookie printed by curl in next command:
curl -w'\n' -b session=eyJ1c2VyX2lkIjoyfQ.CkgjFw.GvpcfFmT0Pm-pPQU66hQf1DPsEo http://localhost:5000/user-check
