angular.module('app', [])
.controller('Controller', ['$scope', '$http', function($scope, $http) {

    var pr = function(args){
        console.log(args);
    };


    var default_panel_after_signin = 'login_mode';


    var init = function() {
        $scope.show_panel('games');
        // $scope.startup.load();
    };


    $scope.startup = (function(){
        var self = {};
        self.fast_load = false;


        self.load = function(){
            $scope.show_panel('loading');
            self.start();
        };


        self.start = function(){
            setTimeout(function(){
                if (self.fast_load == false){
                    self.fast_load = true;
                    $scope.show_panel('login_mode');
                    $scope.$apply();
                }
            }, 10000);
        };


        self.stop = function(){
            self.fast_load = true;
            $scope.show_panel('login_mode');
        };


        return self;
    })();


    $scope.show_panel = function(panel_name) {
        $scope.panel = panel_name;
        console.log($scope.panel);
    };


    $scope.user = (function() {

        var self = {};
        self.username = 'Guest';
        self.password = '';
        self.signed_in = false;
        self.invalid_userpass = false;
        self.wait_for_response = false;


        self.signin = function() {
            var data = {
                username: self.username,
                password: self.password,
            };
            self.password = '';
            self.signed_in = false;
            self.invalid_userpass = false;
            self.wait_for_response = true;
            $http.post('/user/signin', data).success(function(res) {
                $scope.show_panel('games');
                self.signed_in = true;
                self.wait_for_response = false;
            })
            .error(function(res) {
                self.invalid_userpass = true;
                self.wait_for_response = false;
            });
        };


        self.signout = function() {
            $http.post('/user/signout').success(function() {
                self.signed_in = false;
                $scope.panel = 'login_mode';
            })
            .error(function(res) {
                alert('اشکال در خروج از سیستم');
            });
        };

        self.guest_mode = function(){
            self.signed_in = true;
            var data = {
                username: 'Guest',
                password: 'Nothing',
            };
            self.signed_in = false;
            self.invalid_userpass = false;
            self.wait_for_response = true;
            $http.post('/user/signin', data).success(function(res) {
                $scope.show_panel('games');
                self.signed_in = true;
                self.wait_for_response = false;
            })
            .error(function(res) {
                self.wait_for_response = false;
            });
        };

        return self;

    })();


    init();

}]);
